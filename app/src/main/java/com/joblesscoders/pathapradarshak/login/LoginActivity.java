package com.joblesscoders.pathapradarshak.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.joblesscoders.pathapradarshak.R;
import com.joblesscoders.pathapradarshak.map.MainActivity;
import com.joblesscoders.pathapradarshak.pojo.User;
import com.joblesscoders.pathapradarshak.utils.RestApiHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean isPolice = false;
    private EditText password,username;
    private View login;
    private  RestApiHandler restApiHandler;
    private RestApiHandler.RetrofitHandler retrofit;
    private String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
         type = getIntent().getExtras().getString("type");
        if(type.equals("police"))
            isPolice = true;

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        login.setOnClickListener(this);
        restApiHandler = new RestApiHandler();
        retrofit = restApiHandler.getRetrofit();



    }

    @Override
    public void onClick(View view) {

        if((username.getText().toString()!=null && username.getText().toString().length()>0) ||(password.getText().toString()!=null && password.getText().toString().length()>0 ))
        {
           // Toast.makeText(this, username.getText().toString()+password.getText()+""+type, Toast.LENGTH_LONG).show();
            User user = new User(username.getText()+"",password.getText()+"",type);
            retrofit.loginUser(user).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if(response.code() == 200){
                        User.setUser(getApplicationContext(),response.body());
                        Toast.makeText(LoginActivity.this, "Successfully logged in", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("type",type);
                        Log.i("hello","kklll"+type);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "User not found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(this, "Username or Password cannot be null", Toast.LENGTH_SHORT).show();
        }

    }
}
