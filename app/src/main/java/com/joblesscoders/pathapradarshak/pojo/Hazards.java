package com.joblesscoders.pathapradarshak.pojo;

public class Hazards {
    private String _id,type,details;
    private Location location;
    private long timestamp;

    public Hazards(String _id, String type, String details, Location location, long timestamp) {
        this._id = _id;
        this.type = type;
        this.details = details;
        this.location = location;
        this.timestamp = timestamp;
    }


    public String get_id() {
        return _id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }



    public String getDetails() {
        return details;
    }

    public Location getLocation() {
        return location;
    }
}
