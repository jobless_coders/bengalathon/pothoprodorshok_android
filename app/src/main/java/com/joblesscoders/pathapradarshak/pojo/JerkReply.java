package com.joblesscoders.pathapradarshak.pojo;

public class JerkReply {
    public JerkReply(String _id, double value, Location location, long timestamp) {
        this._id = _id;
        this.value = value;
        this.location = location;
        this.timestamp = timestamp;
    }

    private String _id;
    private double value;
    private Location location;
    private  long timestamp;

    public String get_id() {
        return _id;
    }

    public double getValue() {
        return value;
    }

    public Location getLocation() {
        return location;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
