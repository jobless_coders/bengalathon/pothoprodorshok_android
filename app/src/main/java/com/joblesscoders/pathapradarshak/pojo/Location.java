package com.joblesscoders.pathapradarshak.pojo;

public class Location {
    private String type;
    private double[] coordinates;

    public Location(String type, double[] coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public double[] getCoordinates() {
        return coordinates;
    }
    public double getLatitude(){
        return coordinates[0];
    }
    public double getLongitude(){
        return coordinates[1];
    }

}
