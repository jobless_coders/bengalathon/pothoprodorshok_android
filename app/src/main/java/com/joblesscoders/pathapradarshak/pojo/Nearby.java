package com.joblesscoders.pathapradarshak.pojo;

public class Nearby {
private double[] coordinates;
private int radius;

    public Nearby(double[] coordinates, int radius) {
        this.coordinates = coordinates;
        this.radius = radius;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public int getRadius() {
        return radius;
    }
}
