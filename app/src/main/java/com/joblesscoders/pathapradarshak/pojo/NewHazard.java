package com.joblesscoders.pathapradarshak.pojo;

public class NewHazard {
    private double latitude, longitude;
    private long timestamp;
    private String uid;

    public NewHazard(double latitude, double longitude, long timestamp, String uid, String details, String type) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.uid = uid;
        this.details = details;
        this.type = type;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String details;
    private String type;
}
