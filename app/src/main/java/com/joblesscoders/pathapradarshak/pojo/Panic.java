package com.joblesscoders.pathapradarshak.pojo;

public class Panic {
    private double latitude,longitude;
    private String uid;
    private long timestamp;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getUid() {
        return uid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Panic(double latitude, double longitude, String uid) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.uid = uid;
    }

    public Panic(double latitude, double longitude, String uid, long timestamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.uid = uid;
        this.timestamp = timestamp;
    }
}
