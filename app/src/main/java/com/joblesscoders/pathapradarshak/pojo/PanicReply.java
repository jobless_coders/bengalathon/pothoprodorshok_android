package com.joblesscoders.pathapradarshak.pojo;

public class PanicReply {
    private String uid,_id;
    private long timestamp;

    public PanicReply(String uid, String _id, long timestamp, Location location) {
        this.uid = uid;
        this._id = _id;
        this.timestamp = timestamp;
        this.location = location;
    }

    public String getUid() {
        return uid;
    }

    public String get_id() {
        return _id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Location getLocation() {
        return location;
    }

    private Location location;

}
