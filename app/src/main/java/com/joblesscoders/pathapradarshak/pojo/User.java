package com.joblesscoders.pathapradarshak.pojo;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    private String username;
    private String password;
    private String type;
    private String _id;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public User(String username, String password, String type, String _id) {
        this.username = username;
        this.password = password;
        this.type = type;
        this._id = _id;
    }

    public User(String message) {
        this.message = message;
    }

    public User(String username, String password, String type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public String get_id() {
        return _id;
    }
    public static User getUser(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        User user = new User(sharedPreferences.getString("username",null),sharedPreferences.getString("password",null),sharedPreferences.getString("type",null),sharedPreferences.getString("id",null));
        return user;
    }
    public static void setUser(Context context,User user){
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username",user.getUsername());
        editor.putString("password",user.getPassword());
        editor.putString("type",user.getType());
        editor.putString("id",user.get_id());
        editor.commit();
    }
    public static boolean isNewUser(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        String d = sharedPreferences.getString("username",null);
        if(d == null)
        return true;
        else
            return false;
    }
    public static void logoutUser(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
       /* SharedPreferences settings = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        settings.edit().clear().commit();*/
    }
}
