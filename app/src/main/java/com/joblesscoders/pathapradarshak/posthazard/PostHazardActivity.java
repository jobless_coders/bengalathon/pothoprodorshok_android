package com.joblesscoders.pathapradarshak.posthazard;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.button.MaterialButton;
import com.joblesscoders.pathapradarshak.R;
import com.joblesscoders.pathapradarshak.pojo.NewHazard;
import com.joblesscoders.pathapradarshak.pojo.User;
import com.joblesscoders.pathapradarshak.utils.RestApiHandler;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostHazardActivity extends AppCompatActivity {

    private AppCompatSpinner spinner, rbspinner;
    MaterialButton submit;
    double lat, lng;
    String details, type, uid, rbtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_hazard);
        initSpinner();
        final RestApiHandler restApiHandler = new RestApiHandler();
        final RestApiHandler.RetrofitHandler retrofitHandler = restApiHandler.getRetrofit();

        lat = Double.parseDouble(getIntent().getExtras().get("latitude").toString());
        lng = Double.parseDouble(getIntent().getExtras().get("longitude").toString());

        uid = User.getUser(this).get_id();
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                details = ((EditText)findViewById(R.id.description)).getText().toString().trim();
                if (details.length() == 0){
                    Toast.makeText(PostHazardActivity.this, "Please enter details to continue", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(type.contains("Select")){
                    Toast.makeText(PostHazardActivity.this, "Please select a type to continue", Toast.LENGTH_SHORT).show();
                    return;
                }


                if(type.equalsIgnoreCase("roadblock") && rbtype.equalsIgnoreCase("--Select a roadblock type--")) {
                    Toast.makeText(PostHazardActivity.this, "Please select roadblock type to continue", Toast.LENGTH_SHORT).show();
                    return;
                }

                NewHazard hazard = new NewHazard(lat, lng, new Date().getTime(), uid, details, type);
                if(type.equalsIgnoreCase("roadblock"))
                    hazard.setType(type + " (" + rbtype + ")");
                final ProgressDialog pd = new ProgressDialog(PostHazardActivity.this);
                pd.setMessage("Submitting data...");
                pd.show();
                retrofitHandler.postHazards(hazard).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        pd.dismiss();
                      /*  if(response.code()==200)
                            Toast.makeText(PostHazardActivity.this, response.body().getMessage()+"oo", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(PostHazardActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();
                   */
                    }


                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
                if(hazard.getType().equalsIgnoreCase("accident")||hazard.getType().equalsIgnoreCase("theft"))
                postHistory(hazard);
//                    else
//                Toast.makeText(PostHazardActivity.this, hazard.getType()+"kll", Toast.LENGTH_SHORT).show();


    }

    private void postHistory(NewHazard hazard) {
//        Toast.makeText(PostHazardActivity.this, "jj", Toast.LENGTH_SHORT).show();
        RestApiHandler.RetrofitHandler retrofit2 = restApiHandler.getRetrofit2();
        retrofit2.postHistory(hazard).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
//                if(response.code() == 200)
//                {
//                    Toast.makeText(PostHazardActivity.this, ""+response.body().getMessage()+"45", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
        });
    }



    public void initSpinner() {

        spinner = findViewById(R.id.hazard_type);
        rbspinner = findViewById(R.id.roadblock_type);

        final ArrayList<String> rblist = new ArrayList<>();
        rblist.add("--Select a roadblock type--");
        rblist.add("Waterlogged");
        rblist.add("Road under construction");
        rblist.add("Broken trees");

        final ArrayList<String> list = new ArrayList<>();
        list.add("--Select a hazard type--");
        list.add("Accident");
        list.add("Theft");
        list.add("Roadblock");

        ArrayAdapter<String> rbadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, rblist);
        rbadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rbspinner.setAdapter(rbadapter);

        rbspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rbtype = rblist.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                type = list.get(position);

                if(list.get(position).equalsIgnoreCase("Roadblock")) {
                    rbspinner.setVisibility(View.VISIBLE);
                }
                else {
                    rbspinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
