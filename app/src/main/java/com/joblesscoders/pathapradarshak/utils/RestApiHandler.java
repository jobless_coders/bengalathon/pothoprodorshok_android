package com.joblesscoders.pathapradarshak.utils;

import com.google.android.gms.maps.model.LatLng;
import com.joblesscoders.pathapradarshak.pojo.JerkData;
import com.joblesscoders.pathapradarshak.pojo.JerkReply;
import com.joblesscoders.pathapradarshak.pojo.NewHazard;
import com.joblesscoders.pathapradarshak.pojo.Panic;
import com.joblesscoders.pathapradarshak.pojo.PanicReply;
import com.joblesscoders.pathapradarshak.pojo.ScoreHazardReply;
import com.joblesscoders.pathapradarshak.pojo.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class RestApiHandler {
    public RestApiHandler()
    {

    }
    public interface RetrofitHandler
    {
        String BASE_URL="https://bengalathon-node.herokuapp.com/";
        String BASE = BASE_URL;
        //"http://192.168.0.7:5000/";


        @POST("getnearby")
        Call<ScoreHazardReply> getNearby(@Body LatLng latLng);
        @GET("gethazarddata")
        Call<List<NewHazard>> getAllHazards();
        @POST("posthazard")
        Call<User> postHazards(@Body NewHazard hazard);
        @POST("login")
        Call<User> loginUser(@Body User user);
        @POST("postjerk")
        Call<User> postJerk(@Body JerkData data);
        @POST("posthistory")
        Call<User> postHistory(@Body NewHazard newHazard);
        @POST("findhazardandscore")
        Call<ScoreHazardReply> getHazardAndScore(@Body List<LatLng> latLngs);
        @GET("getjerkdata")
        Call<List<JerkReply>> getAllJerkData();
        @POST("postpanic")
        Call<User> postPanicData(@Body Panic panic);
        @GET("getpanic")
        Call<List<PanicReply>> getPanicData();
        @POST("updatepanic")
        Call<User> updatePanic(@Body Panic panic);
        @POST("updatelocation")
        Call<User> updateLocation(@Body Panic user);


    }

    public RetrofitHandler getRetrofit()
    {
        RetrofitHandler retrofitHandler;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitHandler.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitHandler= retrofit.create(RetrofitHandler.class);
        return retrofitHandler;

    }
    public RetrofitHandler getRetrofit2()
    {
        RetrofitHandler retrofitHandler;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitHandler.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitHandler= retrofit.create(RetrofitHandler.class);
        return retrofitHandler;

    }


}
